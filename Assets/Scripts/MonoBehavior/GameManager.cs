using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] FloatingJoystick joystick;
    [SerializeField] Transform pivotCamera;
    [SerializeField] ItemTypeData itemTypeData;
    [SerializeField] LevelData levelData;
    
    PlayerController playerController;

    public static GameManager Instance;

    public FloatingJoystick Joystick { get => joystick; set => joystick = value; }
    public Transform PivotCamera { get => pivotCamera; set => pivotCamera = value; }
    public ItemTypeData ItemTypeData { get => itemTypeData; set => itemTypeData = value; }
    public LevelData LevelData { get => levelData; set => levelData = value; }
    public PlayerController PlayerController { get => playerController; set => playerController = value; }

    private void Awake()
    {
        Application.targetFrameRate = 120;
    }

    public void Prepare()
    {
        Instance = this;
    }
}
