using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SetColliderItem : ComponentItem
{
    Collider[] colliders;
    Rigidbody body;
    BodyType type;
    bool setupComplete;

    public void SetCollider(BodyType bodyType, bool freezeRotateXZ)
    {
        type = bodyType;
        colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
            collider.isTrigger = false;

        body = transform.AddComponent<Rigidbody>();
        body.isKinematic = bodyType == BodyType.noTriggerHaveKinematic;
        body.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        if (freezeRotateXZ) body.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    protected override void OnDeath()
    {
        Debug.Log("Death");
    }

    protected override void OnRevive()
    {
        Debug.Log("Revive");
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (setupComplete) return;
        if(collision.collider.CompareTag("Ground") || collision.collider.CompareTag("Climbing"))
        {
            if(type == BodyType.isTrigger)
            {
                Destroy(body);
                foreach (Collider collider in colliders) collider.isTrigger = true;
            }
            else if(type == BodyType.noTriggerHaveKinematic) 
            {
                body.isKinematic = true;
            }

            setupComplete = true;
        }
    }*/
}
