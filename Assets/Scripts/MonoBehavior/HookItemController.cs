using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Core;
using Unity.Mathematics;
using UnityEngine;
using static LevelData;

public class HookItemController : MonoBehaviour
{
    public float currentLevel;
    public float currentExp;
    public Item[] itemsHookedUnsort;

    public float speedRate;
    public float rotateSpeedRate;
    public float radius;

    private void Start()
    {
        radius = 0.5f;
        speedRate = 1f;
        rotateSpeedRate = 1f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Item"))
        {
            Item itemData = collision.collider.GetComponent<Item>();
            if (itemData.isDeath || currentLevel < itemData.requiredLevel) return;

            GetItem(itemData);
        }
        else if(collision.collider.CompareTag("Item Child"))
        {
            Item itemData = collision.collider.transform.parent.GetComponent<Item>();
            if (itemData.isDeath || currentLevel < itemData.requiredLevel) return;

            GetItem(itemData);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            Item itemData = other.GetComponent<Item>();
            if (itemData.isDeath || currentLevel < itemData.requiredLevel) return;

            GetItem(itemData);
        }
        else if (other.CompareTag("Item Child"))
        {
            Item itemData = other.transform.parent.GetComponent<Item>();
            if (itemData.isDeath || currentLevel < itemData.requiredLevel) return;

            GetItem(itemData);
        }
    }

    void GetItem(Item item)
    {
        item.isDeath = true;
        item.transform.SetParent(transform, true);

        item.gameObject.layer = 7;
        item.Death();

        currentExp += item.upExp;
        SetLevel();
        SaveAllItemsHooked();
    }

    void SetLevel()
    {
        LevelManager levelData = GameManager.Instance.LevelData.levelDatas.Where(level => currentExp >= level.RequiredExp).LastOrDefault();
        currentLevel = levelData == null ? 0 : levelData.Level;
        CameraController.Instance.CheckForZoomOut(levelData == null ? 0f : levelData.ZoomOutUnit);
        radius = levelData == null ? 0.5f : levelData.Radius;
        rotateSpeedRate = levelData == null ? 1f : levelData.ReducedSpeedRotate / 100f;
        speedRate = levelData == null ? 1f : levelData.UpSpeed;
    }

    void SaveAllItemsHooked()
    {
        itemsHookedUnsort = transform.GetComponentsInChildren<Item>();
        FixPhysics();
        FixRender();
    }

    void FixPhysics()
    {
        for (int i = 0; i < itemsHookedUnsort.Length; i++)
        {
            if (currentLevel - itemsHookedUnsort[i].requiredLevel > 2) itemsHookedUnsort[i].OnOffCollider(false);
        }
    }

    void FixRender()
    {
        int totalItem = itemsHookedUnsort.Length;

        if (totalItem > 75)
        {
            int totalHide = totalItem - 50;

            for (int i = 0; i < totalItem; i++)
            {
                Transform item = itemsHookedUnsort[i].transform;
                Item itemController = item.GetComponent<Item>();

                if (item.CompareTag("Item"))
                {
                    if (totalHide > 0 && currentLevel - itemController.requiredLevel > 4)
                    {
                        item.gameObject.SetActive(false);
                        totalHide--;
                    }
                    else item.gameObject.SetActive(true);
                }
            }
        }
    }

}