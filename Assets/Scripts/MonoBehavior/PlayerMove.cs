using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] Transform character;

    HookItemController controller;
    Animator characterAnimator;

    private void Start()
    {
        characterAnimator = character.GetComponent<Animator>();
        controller = GetComponent<HookItemController>();
    }

    public void RollByDirection(float3 direction, float DeltaTime)
    {
        Vector2 input = GameManager.Instance.Joystick.Direction;
        Vector3 directionRot = new(direction.z / 360f, 0f, direction.x * -1f / 360f);

        float speedRot = 400f;
        Transform cameraPivot = GameManager.Instance.PivotCamera;
        Vector3 globalRotationAxis = cameraPivot.TransformDirection(directionRot);
        transform.RotateAround(transform.position, globalRotationAxis, controller.rotateSpeedRate * speedRot * DeltaTime);

        if(Mathf.Abs(input.x) + Mathf.Abs(input.y) > 0f)
        {
            Vector3 globalLookDirection = cameraPivot.TransformDirection(direction);
            Quaternion rotGoal = Quaternion.LookRotation(globalLookDirection);
            character.rotation = Quaternion.Slerp(character.rotation, rotGoal, 0.2f);
            characterAnimator.SetBool("run", true);
        }
        else characterAnimator.SetBool("run", false);
    }
}
