using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

public abstract class ComponentItem : MonoBehaviour
{
    [HideInInspector] public Item itemController;

    private void Start()
    {
        itemController = GetComponent<Item>();

        itemController.OnRevive += OnRevive;
        itemController.OnDeath += OnDeath;
    }

    protected abstract void OnDeath();
    protected abstract void OnRevive();
}
