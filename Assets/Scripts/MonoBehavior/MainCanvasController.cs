using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainCanvasController : MonoBehaviour
{
    public void LoadScene(int index)
    {
        World world = World.DefaultGameObjectInjectionWorld;
        EntityManager entityManager = world.EntityManager;
        EntityQuery allEntitiesQuery = entityManager.UniversalQuery;
        entityManager.DestroyEntity(allEntitiesQuery);

        SceneManager.LoadScene(index);
    }

    public void RotateCamera()
    {
        Transform view = GameManager.Instance.PlayerController.transform.GetChild(0).GetChild(2);

        Vector3 rot = view.rotation.eulerAngles;
        GameManager.Instance.PivotCamera.transform.DORotate(rot, 0.5f);
    }
}
