using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static ItemTypeData;

public class Item : MonoBehaviour
{
    public string idItem;
    public string idType;
    public bool isDeath;

    public float requiredLevel;
    public float upExp;
    Collider[] colliders;

    float distanceToHookedParent;
    HookItemController parentHooked;

    public delegate void OnStateChange();
    public OnStateChange OnDeath, OnRevive;

    public Collider[] Colliders { get => colliders; set => colliders = value; }

    private void FixedUpdate()
    {
        
        if (isDeath)
        {
            float currentDistance = Vector3.Distance(transform.position, parentHooked.transform.position);
            if(currentDistance > distanceToHookedParent)
            {
                Vector3 dir = parentHooked.transform.position - transform.position;
                transform.position += Time.fixedDeltaTime * dir.normalized;
            }
           
        }
    }

    public void Prepare()
    {
        Colliders = GetComponentsInChildren<Collider>();
        
        ItemType itemType = GameManager.Instance.ItemTypeData.itemTypes.Where(item => item.Id == idType).FirstOrDefault();
        if (itemType == null) return;
        requiredLevel = itemType.RequiredMinLevel;
        upExp = itemType.UpExp;

        if(TryGetComponent<AddComponentManager>(out var acm)) acm.Prepare();
    }

    public void OnOffCollider(bool isOn)
    {
        foreach(Collider collider in Colliders)
        {
            collider.enabled = isOn;
        }
    }

    public void Death()
    {
        parentHooked = transform.parent.GetComponent<HookItemController>();
        distanceToHookedParent = parentHooked.radius / 2f;

        OnDeath?.Invoke();
    }
}
