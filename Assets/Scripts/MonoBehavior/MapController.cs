using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    [SerializeField] private Transform spawnPoints;
    [SerializeField] private Item[] items;

    public static MapController Instance;

    public Transform SpawnPoints { get => spawnPoints; set => spawnPoints = value; }
    public Item[] Items { get => items; set => items = value; }

    public void PrepareData()
    {
        Instance = this;
        Items = GetComponentsInChildren<Item>();
    }
}
