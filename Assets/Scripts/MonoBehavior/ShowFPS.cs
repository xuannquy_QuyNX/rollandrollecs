using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowFPS : MonoBehaviour
{
    private TMP_Text _fpsText;
    private void Start()
    {
        _fpsText = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        _fpsText.text = Mathf.RoundToInt(1f / Time.deltaTime) + " FPS";
    }
}
