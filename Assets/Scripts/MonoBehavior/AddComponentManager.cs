﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AddComponentManager : MonoBehaviour
{
    [Tooltip("Dạng va chạm của item")]
    [SerializeField] BodyType bodyType;

    [Tooltip("Đóng băng trục xoay nào để không bị đổ nếu là No Trigger And Kinematic")]
    [SerializeField] bool freezeRotateXZ;

    bool isAutoSpawn;

    public BodyType BodyType { get => bodyType; set => bodyType = value; }
    public bool IsAutoSpawn { get => isAutoSpawn; set => isAutoSpawn = value; }

    /*// Start is called before the first frame update
    void Start()
    {
        if (IsAutoSpawn) return;
        LoadingComponent();
    }*/

    public void Prepare()
    {
        if (IsAutoSpawn) return;
        LoadingComponent();
    }

    public void LoadingComponent()
    {
        if(bodyType != BodyType.isTrigger)
        {
            SetColliderItem colliderManager = transform.AddComponent<SetColliderItem>();
            colliderManager.SetCollider(bodyType, freezeRotateXZ);
        }
        else
        {
            Item itemController = GetComponent<Item>();
            foreach (Collider collider in itemController.Colliders) collider.isTrigger = true;
        }

        /*if (stopAnimation)
        {
            transform.AddComponent<SetAnimatorDeath>();
        }*/
    }
}

public enum BodyType
{
    isTrigger, noTriggerAndKinematic, noTriggerHaveKinematic
}
