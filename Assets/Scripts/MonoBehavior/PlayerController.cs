using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Transform sphere, bug;

    public float speed;

    public void RollSphere(float3 dirMove, float DeltaTime)
    {
        Vector3 directionRot = new(dirMove.z / 360f, 0f, dirMove.x * -1f / 360f);
        Vector3 globalRotationAxis = sphere.TransformDirection(directionRot);

        sphere.RotateAround(sphere.transform.position, globalRotationAxis, 2f * DeltaTime);
    }
}
