using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Vector3 offsetPos;
    Vector3 offsetTarget, offsetRaw;

    public static CameraController Instance;

    public Vector3 OffsetTarget { get => offsetTarget; set => offsetTarget = value; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        offsetRaw = offsetPos;
        OffsetTarget = offsetRaw;
    }

    public void CheckForZoomOut(float zoomOutUnit)
    {
        OffsetTarget = offsetRaw + offsetRaw * zoomOutUnit;
    }
}
