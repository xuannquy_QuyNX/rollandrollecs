﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "Commons/Level")]
public class LevelData : ScriptableObject
{
    [Serializable]
    public class LevelManager
    {
        [Tooltip("Không được trùng lặp")]
        [SerializeField] private int level;

        [Tooltip("Số điểm kinh nghiệm tối thiểu để đạt level này")]
        [SerializeField] private float requiredExp;

        [Tooltip("Khoảng cách tối đa của vật thể bị hút tới quả cầu (Level 0 là 0.5)")]
        [SerializeField] private float radius;

        [Tooltip("Tốc độ tăng gấp mấy lần so với ban đầu")]
        [SerializeField] private float upSpeed;

        [Tooltip("Tốc độ xoay còn bao nhiêu % so với ban đầu")]
        [Range(0f, 100f)]
        [SerializeField] private float reducedSpeedRotate;

        [Tooltip("Camera sẽ lùi bao nhiêu đơn vị so với ban đầu khi đạt level này (Level 0 là 1)")]
        [SerializeField] private float zoomOutUnit;

        public int Level { get => level; set => level = value; }
        public float RequiredExp { get => requiredExp; set => requiredExp = value; }
        public float Radius { get => radius; set => radius = value; }
        public float UpSpeed { get => upSpeed; set => upSpeed = value; }
        public float ZoomOutUnit { get => zoomOutUnit; set => zoomOutUnit = value; }
        public float ReducedSpeedRotate { get => reducedSpeedRotate; set => reducedSpeedRotate = value; }
    }

    public List<LevelManager> levelDatas;
}
