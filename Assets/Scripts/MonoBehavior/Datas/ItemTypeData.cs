﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemTypeData", menuName = "Commons/Item Type")]
public class ItemTypeData : ScriptableObject
{
    [Serializable]
    public class ItemType
    {
        [Tooltip ("ID của item type phải là duy nhất")]
        [SerializeField] private string id;

        [Tooltip("Level tối thiểu để ăn được item type này trong game")]
        [SerializeField] private float requiredMinLevel;

        [Tooltip("Số exp nhận thêm sau khi ăn item này")]
        [SerializeField] private float upExp;

        public string Id { get => id; set => id = value; }
        public float RequiredMinLevel { get => requiredMinLevel; set => requiredMinLevel = value; }
        public float UpExp { get => upExp; set => upExp = value; }
    }

    public ItemType[] itemTypes;
}
