using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(GetValuePlayerDirectionMove))]
public partial class PlayerKeepOnPostionSystem : SystemBase
{
    protected override void OnUpdate()
    {
        EntityCommandBuffer ecb = new(Unity.Collections.Allocator.TempJob);
        foreach (var (transform, playerTransform, direction, entity) in SystemAPI.Query<LocalTransform, PlayerTransform, PlayerMoveDirection>().WithEntityAccess())
        {
            try
            {
                float3 currentPostion = transform.Position;
                playerTransform.Player.position = currentPostion;

                Transform deathStar = playerTransform.Player.transform.GetChild(1);

                Vector3 positionBall = deathStar.position;
                positionBall.y = GameManager.Instance.PivotCamera.position.y;
                GameManager.Instance.PivotCamera.position = positionBall;

                PlayerMove playerMoveController = deathStar.GetComponent<PlayerMove>();
                playerMoveController.RollByDirection(direction.Value, SystemAPI.Time.DeltaTime);

                Transform bug = playerTransform.Player.transform.GetChild(0);
                currentPostion = deathStar.position;
                currentPostion.y += 0.25f;

                bug.position = currentPostion;
            }
            catch
            {
                Debug.Log("Not ready");
            }
        }


        ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
        ecb.Dispose();
    }
}
