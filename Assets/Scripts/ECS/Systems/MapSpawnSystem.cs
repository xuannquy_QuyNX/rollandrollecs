using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public partial class MapSpawnSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<MapSpawnPrefab>();
    }
    protected override void OnUpdate()
    {
        EntityCommandBuffer ecb = new(Allocator.Temp);

        foreach (var(mapSpawn, entity) in SystemAPI.Query<MapSpawnPrefab>().WithEntityAccess())
        {
            GameObject prefab = mapSpawn.mapPrefab;
            GameObject map = Object.Instantiate(prefab);
            MapController mapController = map.GetComponent<MapController>();
            mapController.PrepareData();

            GameManager gameManager = Object.FindAnyObjectByType<GameManager>();
            gameManager.Prepare();

            SpawnItemMager(ecb, mapController.Items);

            ecb.DestroyEntity(entity);
        }

        ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
        ecb.Dispose();
    }

    void SpawnItemMager(EntityCommandBuffer ecb, Item[] items)
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].Prepare();

            Entity itemEntity = ecb.CreateEntity();
            ecb.SetName(itemEntity, "Item " + i);

            ecb.AddComponent(itemEntity, new ItemTag
            {
                item = items[i]
            });

            ecb.AddComponent(itemEntity, new LocalTransform
            {
                Position = items[i].transform.position
            });
        }
    }
}
