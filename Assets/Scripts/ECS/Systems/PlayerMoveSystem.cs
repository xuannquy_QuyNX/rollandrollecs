using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(GetValuePlayerDirectionMove))]
public partial class PlayerMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = SystemAPI.Time.DeltaTime;

        new MovePlayerEntityJob
        {
            DeltaTime = deltaTime
        }.Schedule();
    }
}

[BurstCompile]
public partial struct MovePlayerEntityJob : IJobEntity
{
    public float DeltaTime;
    
    public void Execute(ref LocalTransform transform, in PlayerMoveDirection direction, in PlayerMoveSpeedRate rate, PlayerMoveSpeed speed)
    {
        transform.Position += speed.Value * rate.Value * DeltaTime * direction.Value;
    }
}