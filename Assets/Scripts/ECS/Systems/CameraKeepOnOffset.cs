using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(CameraGetValueOffset))]
public partial class CameraKeepOnOffset : SystemBase
{
    protected override void OnUpdate()
    {
        EntityCommandBuffer ecb = new(Unity.Collections.Allocator.TempJob);

        foreach (var (transform, offset, entity) in SystemAPI.Query<LocalTransform, CameraOffset>().WithEntityAccess())
        {
            try
            {
                float3 offsetTarget = transform.Position;
                Camera.main.transform.localPosition = offsetTarget;
            }
            catch { Debug.Log("Not ready"); }
        }

        ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
        ecb.Dispose();
    }
}
