using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(CameraGetValueOffset))]
public partial class CameraSmothOffsetSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = SystemAPI.Time.DeltaTime;

        new CameraSmothOffsetJob
        {
            DeltaTime = deltaTime
        }.Schedule();
    }
}

[BurstCompile]
public partial struct CameraSmothOffsetJob : IJobEntity
{
    public float DeltaTime;

    public void Execute(ref LocalTransform transform, in CameraOffset offset)
    {
        float3 dir = offset.Value - transform.Position;
        transform.Position += DeltaTime * dir;
    }
}