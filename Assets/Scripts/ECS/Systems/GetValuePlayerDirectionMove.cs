using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(PlayerSpawnSystem))]
public partial class GetValuePlayerDirectionMove : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<PlayerMoveDirection>();
        RequireForUpdate<PlayerMoveSpeedRate>();
    }

    protected override void OnUpdate()
    {
        float2 input = GameManager.Instance.Joystick.Direction;
        float3 moveDirection = new(input.x, 0f, input.y);

        Vector3 globalOffset = GameManager.Instance.PivotCamera.TransformPoint(moveDirection);

        Transform ball = GameManager.Instance.PlayerController.transform.GetChild(1);
        Vector3 dir = globalOffset - ball.position;
        float speedRate = ball.GetComponent<HookItemController>().rotateSpeedRate;

        SystemAPI.SetSingleton(new PlayerMoveDirection
        {
            Value = dir
        });

        SystemAPI.SetSingleton(new PlayerMoveSpeedRate
        {
            Value = speedRate
        });
    }

}