using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(MapSpawnSystem))]
public partial class PlayerSpawnSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<PlayerSpawn>();
    }

    protected override void OnUpdate()
    {
        EntityCommandBuffer ecb = new(Unity.Collections.Allocator.Temp);

        foreach(var(playerSpawn, entity) in SystemAPI.Query<PlayerSpawn>().WithNone<PlayerTag>().WithEntityAccess())
        {
            float3 positionSpawn = MapController.Instance.SpawnPoints.GetChild(0).position;
            GameObject player = Object.Instantiate(playerSpawn.Prefab, positionSpawn, quaternion.identity);
            PlayerController playerController = player.GetComponent<PlayerController>();
            GameManager.Instance.PlayerController = playerController;

            ecb.RemoveComponent<PlayerSpawn>(entity);
            ecb.AddComponent<PlayerTag>(entity);
            ecb.AddComponent<PlayerMoveDirection>(entity);
            ecb.AddComponent<PlayerMoveSpeedRate>(entity);

            ecb.AddComponent(entity, new PlayerMoveSpeed
            {
                Value = playerController.speed
            });

            ecb.AddComponent(entity, new PlayerTransform
            {
                Player = player.transform
            });

            LocalTransform firstFirstTransform = new()
            {
                Position = positionSpawn
            };
            ecb.SetComponent(entity, firstFirstTransform);
        }

        ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
        ecb.Dispose();
    }
}

[BurstCompile]
public partial struct PlayerMoveJob : IJobEntity
{
    public float DeltaTime;

    public void Execute(ref LocalTransform transform, in PlayerMoveDirection direction, PlayerMoveSpeed speed)
    {
        transform.Position += speed.Value * DeltaTime * direction.Value;
    }
}
