using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[UpdateAfter(typeof(MapSpawnSystem))]
public partial class CameraGetValueOffset : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<CameraOffset>();
    }

    protected override void OnUpdate()
    {
        var ecb = new EntityCommandBuffer(Unity.Collections.Allocator.Temp);

        float3 offset = Camera.main.GetComponent<CameraController>().OffsetTarget;

        SystemAPI.SetSingleton(new CameraOffset
        {
            Value = offset
        });

        ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
        ecb.Dispose();
    }
}
