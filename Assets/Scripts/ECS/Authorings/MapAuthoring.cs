using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class MapAuthoring : MonoBehaviour
{
    public GameObject mapPrefab;
}

public class MapAuthoringBaker : Baker<MapAuthoring>
{
    public override void Bake(MapAuthoring authoring)
    {
        var entity = GetEntity(TransformUsageFlags.Renderable);
        AddComponentObject(entity, new MapSpawnPrefab
        {
            mapPrefab = authoring.mapPrefab
        });
    }
}
