using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class PlayerSpawn : IComponentData 
{
    public GameObject Prefab;
}

public struct PlayerTag : IComponentData { }
public class PlayerTransform : IComponentData
{
    public Transform Player;
}
public struct PlayerMoveSpeed : IComponentData 
{
    public float Value;
}

public struct PlayerMoveDirection: IComponentData 
{
    public float3 Value;
}

public struct PlayerMoveSpeedRate : IComponentData
{
    public float Value;
}