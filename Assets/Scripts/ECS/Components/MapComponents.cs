using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class MapSpawnPrefab : IComponentData
{
    public GameObject mapPrefab;
}

public class ItemTag : IComponentData 
{
    public Item item;
}