using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public struct CameraOffset : IComponentData
{
    public float3 Value;
}
