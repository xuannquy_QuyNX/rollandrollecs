using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity *= 5f;

        AsyncOperation ao = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        StartCoroutine(NextScene(ao));
    }

    IEnumerator NextScene(AsyncOperation asyncOperation)
    {
        while (true)
        {
            if (asyncOperation.progress >= 0.9f)
            {
                SceneManager.UnloadSceneAsync(0, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
            yield return null;
        }
    }
}
